package problem;

import java.math.BigInteger;

public class P65 {
    public static void main(String[] args) {
        BigInteger tuso;
        BigInteger mauso;
        int n = 1000, k = (n - 1) / 3;
        if (n % 3 == 0) {
            tuso = BigInteger.valueOf(2 * k + 2);
            mauso = BigInteger.valueOf(2 * k + 3);
        } else if (n % 3 == 1) {
            tuso = BigInteger.ZERO;
            mauso = BigInteger.ONE;
        } else {
            tuso = BigInteger.ONE;
            mauso = BigInteger.ONE;
        }
        for (int i = k; i > 0; i--) {
            BigInteger t = tuso;
            BigInteger m = mauso;
            tuso = BigInteger.valueOf(2 * i).multiply(t.add(m)).add(m);
            mauso = BigInteger.valueOf(2 * i).multiply(t.add(m)).add(t).add(BigInteger.valueOf(2).multiply(m));
        }
        tuso = BigInteger.valueOf(2).multiply(mauso).add(tuso);
        System.out.println("tu so cua phan so thu " + n + " la:\n" + tuso);
        System.out.println("mau so cua phan so thu " + n + " la:\n" + mauso);
    }
}
