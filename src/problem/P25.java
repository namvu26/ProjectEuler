package problem;

import java.math.BigInteger;

public class P25 {
    public static void main(String[] args) {
        // tim so Fibonaci dau tien co dung 1000 chu so
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ONE;
        int count = 2;
        while (true) {
            BigInteger temp = a;
            a = b;
            b = a.add(temp);
            count++;
            if (b.toString().length() == 1000) break;
        }
        System.out.println(count);
    }
}
