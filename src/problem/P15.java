package problem;

import java.math.BigInteger;

public class P15 {
    // tim so cach di tu dinh cua hinh vuong 20x20 den dinh doi dien
    // so cach di la 40!/(20!*20!)
    public static void main(String[] args) {
        BigInteger a = BigInteger.ONE;
        BigInteger b = BigInteger.ONE;
        for (int i = 1; i <= 20; i++)
            a = a.multiply(BigInteger.valueOf(i));
        b = a;
        for (int i = 21; i <= 40; i++)
            b = b.multiply(BigInteger.valueOf(i));
        System.out.println(b.divide(a.multiply(a)));
    }
}
