package problem;

public class P71 {
    public static void main(String[] args) {
        long a = 3, b = 7;
        long m = 0, n = 1;
        for (int j = 1000000; j > 2; j--) {
            long i = (a * j - 1) / b;
            if (i * n > j * m) {
                m = i;
                n = j;
            }
        }
        System.out.println(m);
        System.out.println(n);
    }
}
