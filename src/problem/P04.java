package problem;

public class P04 {
    public static void main(String[] args) {
        // tim so Palindromic lon nhat duoc tao boi tich cua 2 so 3 chu so
        int max = 0;
        for (int i = 100; i < 1000; i++)
            for (int j = 100; j < 1000; j++)
                if (Ham.KtraPalindromicNumber(i * j) && i * j > max) max = i * j;
        System.out.println(max);
    }
}
