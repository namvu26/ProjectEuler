package problem;

public class P72 {
    public static boolean isNgtoCungnhau(int a, int b) {
        while (a * b != 0) {
            if (a > b) a = a % b;
            else b = b % a;
        }
        if (a + b != 1) return false;
        return true;
    }

    public static void main(String[] args) {
        int sum = 0;
        for (int i = 2; i <= 1000000; i++)
            for (int j = 1; j < i; j++)
                if (isNgtoCungnhau(i, j)) sum++;
        System.out.println(sum);
    }
}
