package problem;

import java.math.BigInteger;

public class P16 {
    // tim tong cac chu so cua 2^1000
    public static void main(String[] args) {
        BigInteger a = BigInteger.ONE;
        for (int i = 1; i <= 1000; i++)
            a = a.multiply(BigInteger.valueOf(2));
        int sum = 0;
        for (int i = 0; i < a.toString().length(); i++)
            sum += a.toString().charAt(i) - '0';
        System.out.println(sum);
    }
}
