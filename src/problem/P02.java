package problem;

public class P02 {
    public static void main(String[] args) {
        // tim tong cac so Fibonaci chan nho hon 4000000
        int a = 1, b = 2;
        int sum = 2;
        while (b < 4000000) {
            int temp = a;
            a = b;
            b = temp + a;
            if (b % 2 == 0) sum += b;
        }
        System.out.println(sum);
    }
}
