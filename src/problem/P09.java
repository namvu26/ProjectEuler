package problem;

public class P09 {
    public static void main(String[] args) {
        // tim bo ba so Pytago co tong bang 1000
        for (int a = 1; a < 333; a++)
            for (int b = a + 1; b < (1000 - a) / 2; b++) {
                int c = 1000 - a - b;
                if (a * a + b * b == c * c) System.out.println(a + " " + b + " " + c);
            }
    }
}
