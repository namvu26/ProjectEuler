package problem;

public class P05 {
    public static void main(String[] args) {
        // tim BCNN cua cac so tu 1 den 20
        int result = 1;
        for (int i = 2; i < 20; i++)
            if (Ham.KtraNguyenTo(i)) {
                int temp = 1;
                while (temp * i < 20)
                    temp *= i;
                result *= temp;
            }
        System.out.println(result);
    }
}
