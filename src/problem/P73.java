package problem;

public class P73 {
    public static boolean isNgtoCungnhau(int a, int b) {
        while (a * b != 0) {
            if (a > b) a = a % b;
            else b = b % a;
        }
        if (a + b != 1) return false;
        return true;
    }

    public static void main(String[] args) {
        int sum = 0;
        for (int d = 12000; d > 3; d--)
            for (int n = d / 3 + 1; n <= d / 2; n++)
                if (isNgtoCungnhau(n, d)) sum++;
        System.out.println(sum);
    }
}
