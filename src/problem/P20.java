package problem;

import java.math.BigInteger;

public class P20 {
    public static void main(String[] args) {
        // tim tong cac chu so cua 100!
        BigInteger a = BigInteger.ONE;
        for (int i = 2; i <= 100; i++)
            a = a.multiply(BigInteger.valueOf(i));
        int sum = 0;
        for (int i = 0; i < a.toString().length(); i++)
            sum += a.toString().charAt(i) - '0';
        System.out.println(sum);
    }
}
