package problem;

public class Ham {
    public int[] PhanTichNhanTu(int n) {
        int i = 3;
        int[] A = new int[32];
        int k = 0;
        while (n % 2 == 0) {
            A[k++] = 2;
            n /= 2;
        }
        while (n != 1) {
            if (n % i == 0) {
                A[k++] = i;
                n /= i;
            } else i += 2;
        }
        int[] B = new int[k];
        for (int j = 0; j < k; j++) B[j] = A[j];
        return B;
    }

    public static boolean KtraPalindromicNumber(int n) {
        String s = String.valueOf(n);
        for (int i = 0; i < s.length() / 2; i++)
            if (s.charAt(i) != s.charAt(s.length() - 1 - i)) return false;
        return true;
    }

    public static boolean KtraNguyenTo(int n) {
        if (n == 0 || n == 1) return false;
        if (n == 2) return true;
        if (n % 2 == 0) return false;
        for (int i = 3; i <= Math.floor(Math.sqrt(n)); i += 2)
            if (n % i == 0) return false;
        return true;
    }

    public static int TriangleNumber(int n) {
        // so triangle thu n la tong cac so tu 1 den n
        return n * (n + 1) / 2;
    }

    public static int TinhSLNhanTu(int n) {
        // tinh so uoc so cua n
        int result = 1;
        int temp = 0, i = 3;
        while (n % 2 == 0) {
            temp++;
            n /= 2;
        }
        result *= temp + 1;
        while (n != 1) {
            temp = 0;
            while (n % i == 0) {
                temp++;
                n /= i;
            }
            result *= temp + 1;
            i += 2;
        }
        return result;
    }
}
