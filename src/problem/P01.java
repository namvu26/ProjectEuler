package problem;

public class P01 {
    public static void main(String[] args) {
        // tim tong cac so tu nhien nho hon 1000 chia het cho 3 hoac 5
        int sum = 0;
        for (int i = 0; i < 1000; i++)
            if (i % 3 == 0 || i % 5 == 0) sum += i;
        System.out.println(sum);
    }
}
