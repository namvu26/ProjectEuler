package problem;

public class P14 {
    public static void main(String[] args) {
        // mot so bat ky neu chan thi chia cho 2, neu le thi nhan 3 cong 1
        // den khi bang 1 thi dung lai, chuoi cac so tren goi la chuoi Collazt
        // tim so co do dai chuoi Collazt lon nhat
        int number = 1000000;
        int max = 0, k = 0;
        long n;
        int[] A = new int[number];
        A[1] = 1;
        for (int i = 2; i < number; i++) {
            n = i;
            int count = 0;
            while (n != 1 && n >= i) {
                if (n % 2 == 0) n /= 2;
                else n = n * 3 + 1;
                count++;
            }
            A[i] = count + A[(int) n];
            if (A[i] > max) {
                max = A[i];
                k = i;
            }
        }
        System.out.println(k);
    }
}
