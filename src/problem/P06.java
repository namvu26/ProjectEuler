package problem;

public class P06 {
    public static void main(String[] args) {
        // tim hieu cua binh phuong tong va tong cac binh phuong cua cac so tu 1 den 1000
        int sum = 0, sumSquares = 0;
        for (int i = 0; i <= 100; i++) {
            sum += i;
            sumSquares += i * i;
        }
        System.out.println(sum * sum - sumSquares);
    }
}
