package problem;

public class P03 {
    public static void main(String[] args) {
        // tim nhan tu lon nhat cua so 600851475143L
        int i = 3;
        long n = 600851475143L;
        int k = 1;
        while (n != 1)
            if (n % i == 0) {
                k = i;
                n /= i;
            } else i += 2;
        System.out.println(k);
    }
}
